from biosppy.signals import ecg
import numpy as np
import moviepy.editor as mpy
import gizeh as gz
from os.path import join
from scipy.io import loadmat
import matplotlib.pyplot as plt


data_folder = '/user/mmi/emotion/data/EATMINT/Database/Physio/'
file_name = 'D02.mat'

data = loadmat(join(data_folder, file_name))
fs = data['fs'][0][0]
labels = np.array([l.strip() for l in data['label']])
collabData = data['collabData']

part = 1

ecg_idx1 = labels == ('%d-EXG1' % part)
ecg_idx2 = labels == ('%d-EXG2' % part)
ecg_signal = (collabData[:, ecg_idx2] - collabData[:, ecg_idx1])[:,0] # TODO: why is there the 1-EXG? signals twice in the lablels ?
computed_HR = ecg.ecg(ecg_signal, fs, show=False)

frame_rate = 30.
BLACK = (0, 0, 0)
def render_HR(time):
    # Find nearest HR time
    # TODO: find a way to not go trough the whole array since it is ordered
    hr_idx = np.argmin(np.abs(computed_HR['heart_rate_ts'] - time))

    # Create surface TODO: create the object only once and then use /modify them ?
    surface = gz.Surface(150, 150)
    text = gz.text("%d" % computed_HR['heart_rate'][hr_idx], fontfamily="Charter",
                fontsize=80, fontweight='bold', fill=BLACK, xy=(75, 75))
    text.draw(surface)
    return surface.get_npimage(transparent=True)

duration = computed_HR['heart_rate_ts'][-1]  # TODO: use duration of collaboration instead of HR
textVideo_mask = mpy.VideoClip(lambda t: render_HR(t)[:, :, 3] / 255.0, ismask=True, duration=duration)
textVideo = mpy.VideoClip(lambda t: render_HR(t)[:, :, :3], duration=duration).set_mask(textVideo_mask) # TODO: use duration of collaboration instead of HR
heartVideo = mpy.ImageClip('heart.png', duration=duration)
hrVideo =  mpy.CompositeVideoClip([heartVideo.set_position(('center', 'center')), textVideo.set_position(('center', 'center'))])
hrVideo.write_videofile('/tmp/video.mp4', fps=frame_rate)